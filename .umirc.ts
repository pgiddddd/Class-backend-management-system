import { defineConfig } from '@umijs/max';

export default defineConfig({
  antd: {},
  access: {},
  model: {},
  initialState: {},
  request: {},
  layout: {
    title: 'Class Admin',
  },
  hash: true, // 配置是否让生成的文件包含 hash 后缀，通常用于增量发布和避免浏览器加载缓存。
  history: {
    // 配置 history 类型和配置项
    type: 'hash', // 可选 browser、hash 和 memory
  },
  proxy: {
    '/api': {
      target: 'http://192.168.202.182:8081/', //本地联调
      changeOrigin: true,
      pathRewrite: { '^/api': '' },
    },
  },
  routes: [
    {
      path: '/login',
      title: '首页',
      component: '@/pages/Login',
      layout: false,
    },
    {
      path: '/',
      redirect: '/login',
    },
    {
      name: '首页',
      path: '/home',
      component: './Home',
    },
    // {
    //   name: '权限演示',
    //   path: '/access',
    //   component: './Access',
    // },
    {
      name: '教育管理',
      path: '/educationManagement',
      routes: [
        {
          path: '/educationManagement',
          redirect: '/educationManagement/student',
        },
        {
          name: '学生管理',
          path: '/educationManagement/student',
          component: '@/pages/educationManagement/Student',
          access: 'canSeeTeacherOrAdmin',
        },
        {
          name: '教师管理',
          path: '/educationManagement/teacher',
          component: '@/pages/educationManagement/Teacher',
          access: 'canSeeTeacherOrAdmin',
        },
        {
          name: '活动管理',
          path: '/educationManagement/activity',
          component: '@/pages/educationManagement/Activity',
        },
        {
          name: '班级管理',
          path: '/educationManagement/class',
          component: '@/pages/educationManagement/Class',
          access: 'canSeeTeacherOrAdmin',
        },
      ],
    },
    {
      name: '更改密码',
      path: '/changePwd',
      component: '@/pages/ChangePwd',
      layout: false,
    },
  ],
  npmClient: 'pnpm',
});
