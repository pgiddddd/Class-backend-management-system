import { login } from '@/services/login';
import { message } from '@/utils/escapeAntd';
import {
  LockOutlined,
  SwapRightOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { history, useModel } from '@umijs/max';
import { Button, Checkbox, Form, Input } from 'antd';
import { useState } from 'react';
import style from './index.less';

const LoginPage: React.FC = () => {
  const [isRemember, setisRemember] = useState<boolean>(false);
  const { initialState, setInitialState } = useModel('@@initialState');

  const onFinish = async (values: LoginAPI.LoginUserAPI) => {
    const data = await login({ ...values });
    if (data.permission === 0) {
      message.error('您无权限,请联系管理员');
      return;
    }
    if (isRemember) {
      //记住该账户
      localStorage.setItem('loginInfo', JSON.stringify(values));
    } else {
      localStorage.removeItem('loginInfo');
    }
    localStorage.setItem('userInfo', JSON.stringify(data));
    localStorage.setItem('token', data.token);
    setInitialState({
      ...data,
    });
    console.log(initialState);

    history.push('/home');
  };
  return (
    <div className={style.loginWrapper}>
      <div className={style.loginLeft}>
        <div className={style.login}>
          <div className={style.title}>Class Admin</div>
          <p>基于react的班级活动后台管理系统</p>
          <p>React-based class activity management system.</p>
          <div className={style.cardBox}>
            <Form layout="vertical" onFinish={onFinish}>
              <Form.Item
                name="userAccount"
                rules={[{ required: true, message: '请输入登录账号' }]}
                label="Username"
                initialValue={
                  localStorage.getItem('loginInfo')
                    ? JSON.parse(localStorage.getItem('loginInfo') || '')
                        .userAccount
                    : ''
                }
              >
                <Input prefix={<UserOutlined />} placeholder="请输入登录账号" />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[{ required: true, message: '请输入密码' }]}
                label="Password"
                initialValue={
                  localStorage.getItem('loginInfo')
                    ? JSON.parse(localStorage.getItem('loginInfo') || '')
                        .password
                    : ''
                }
              >
                <Input.Password
                  prefix={<LockOutlined />}
                  placeholder="请输入密码"
                />
              </Form.Item>
              <Form.Item>
                <Checkbox
                  name="remember"
                  checked={isRemember}
                  onChange={() => setisRemember(!isRemember)}
                >
                  Remember me
                </Checkbox>
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<SwapRightOutlined />}
                >
                  Login
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
      <div className={style.loginRight}></div>
    </div>
  );
};

export default LoginPage;
