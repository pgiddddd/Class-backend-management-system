import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { PageContainer, ProTable } from '@ant-design/pro-components';
import { Button } from 'antd';
import { useRef } from 'react';

const columns: ProColumns[] = [
  {
    title: '届数',
    dataIndex: 'grade',
  },
  {
    title: '学院',
  },
  {
    title: '班级名称',
  },
  {
    title: '辅导员',
  },
  {
    title: '操作',
    valueType: 'option',
    key: 'option',
    render: () => [
      <a key="edit">编辑</a>,
      <a key="view">详情</a>,
      <a key="delete">删除</a>,
    ],
  },
];

const ClassPage = () => {
  const actionRef = useRef<ActionType>();
  return (
    <PageContainer title={false}>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        cardBordered
        rowKey="id"
        search={{
          labelWidth: 'auto',
        }}
        options={false}
        pagination={{
          pageSizeOptions: [5, 10, 15, 20],
          showSizeChanger: true,
        }}
        dateFormatter="string"
        headerTitle="活动表格"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            onClick={() => {
              actionRef.current?.reload();
            }}
            type="primary"
          >
            新建
          </Button>,
        ]}
      />
    </PageContainer>
  );
};
export default ClassPage;
