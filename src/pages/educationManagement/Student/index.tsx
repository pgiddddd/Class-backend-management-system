import { getClass } from '@/services/classInfo';
import { getAllGrade, resetPassword } from '@/services/comom';
import { getPositionInfo } from '@/services/position';
import { deleteStudentByAccount, getStudent } from '@/services/student';
import { getTeacher } from '@/services/teacher';
import { message } from '@/utils/escapeAntd';
import transformData from '@/utils/formatData';
import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { PageContainer, ProTable } from '@ant-design/pro-components';
import { Button, Modal, Popconfirm } from 'antd';
import { useEffect, useRef, useState } from 'react';
import StudentCreateOrEdit from './components/StudentAddEdit';

const StudentPage = () => {
  const actionRef = useRef<ActionType>();
  //控制编辑和新增的开关
  const [isOpen, setIsOpen] = useState(false);
  //新增还是编辑
  const [drawType, setDrawType] = useState<'create' | 'edit'>('edit');
  //获取职位
  const [position, setPosition] = useState<{ label: string; value: number }[]>(
    [],
  );
  //获取教师列表
  const [teacher, setTeacher] = useState<{ label: string; value: string }[]>(
    [],
  );
  //获取学院和班级信息
  const [classInfo, setClassInfo] = useState([]);
  //设置表格编辑的数据
  const [editData, setEditData] = useState<StudentAPI.StudentAddOrEditType>();
  //获取届数
  const [gradeList, setGradeList] = useState<
    { label: string; value: number }[]
  >([]);

  //获取职位
  const positionList = async () => {
    const arr = await transformData(getPositionInfo);
    setPosition(arr);
  };

  const { confirm } = Modal;
  //获取教师列表
  const teacherList = async () => {
    const data = await getTeacher();
    const arr = data.map((item: TeacherAPI.TeacherAddEditType) => {
      return {
        label: item.userName,
        value: item.userAccount,
      };
    });
    setTeacher(arr);
  };

  //获取学院和班级的列表
  const classList = async () => {
    const data = await getClass();
    setClassInfo(data);
  };

  // 获取届数的下拉框
  const getAllGradeList = async () => {
    const res = await transformData(getAllGrade);
    setGradeList(res);
  };

  useEffect(() => {
    positionList();
    teacherList();
    classList();
    getAllGradeList();
  }, []);

  useEffect(() => {
    if (isOpen === false) {
      actionRef.current?.reload();
    }
  }, [isOpen]);

  const onClickDraw = (
    type: 'edit' | 'create',
    data?: StudentAPI.StudentAddOrEditType,
  ) => {
    setDrawType(type);
    if (type !== 'create') {
      setEditData(data);
    }
    setIsOpen(!isOpen);
  };

  const columns: ProColumns[] = [
    {
      title: '届数',
      dataIndex: 'grade',
      valueType: 'select',
      fieldProps: {
        options: gradeList,
      },
    },
    {
      title: '姓名',
      dataIndex: 'studentName',
    },
    {
      title: '职位',
      dataIndex: 'position',
      valueType: 'select',
      fieldProps: {
        options: position,
      },
    },
    {
      title: '班级',
      dataIndex: 'className',
      hideInSearch: true,
    },
    {
      title: '辅导员',
      dataIndex: 'teacherAccount',
      hideInTable: true,
      fieldProps: {
        options: teacher,
      },
      valueType: 'select',
    },
    {
      title: '辅导员',
      dataIndex: 'teacherName',
      hideInSearch: true,
    },
    {
      title: '所属二级学院及班级',
      dataIndex: 'cascader',
      fieldProps: {
        options: classInfo,
        fieldNames: {
          children: 'classInfoList',
          value: 'id',
          label: 'name',
        },
      },
      valueType: 'cascader',
      hideInTable: true,
    },
    {
      title: '所属二级学院',
      dataIndex: 'facultyName',
      hideInSearch: true,
    },
    {
      title: '操作',
      valueType: 'option',
      key: 'option',
      render: (_, entity) => [
        <>
          <a key="edit" onClick={() => onClickDraw('edit', entity)}>
            编辑
          </a>
          <a
            key="reset"
            onClick={() => {
              confirm({
                title: '重置密码',
                icon: <ExclamationCircleOutlined />,
                content: '重置密码为123456',
                onOk: async () => {
                  const res = await resetPassword(entity.studentAccount);
                  message.info(res);
                },
              });
            }}
          >
            重置密码
          </a>
          <Popconfirm
            title="是否删除该学生"
            okText="确认"
            cancelText="取消"
            onConfirm={async () => {
              const res = await deleteStudentByAccount(entity.studentAccount);
              message.info(res.msg);
            }}
          >
            <a>删除</a>
          </Popconfirm>
        </>,
      ],
    },
  ];
  return (
    <PageContainer title={false}>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        cardBordered
        rowKey="studentAccount"
        search={{
          labelWidth: 'auto',
        }}
        options={false}
        pagination={{
          pageSizeOptions: [5, 10, 15, 20],
          showSizeChanger: true,
        }}
        request={async (params) => {
          let facultiesId = null;
          let classInfoId = null;
          if (params.cascader && params.cascader.length >= 2) {
            facultiesId = params.cascader[0];
            classInfoId = params.cascader[1];
          }
          const data = await getStudent({
            ...params,
            facultiesId: facultiesId,
            classInfoId: classInfoId,
            pageSize: params.pageSize as number,
            pageNumber: params.current as number,
          });
          return {
            data: data.records,
            total: data.total,
          };
        }}
        dateFormatter="string"
        headerTitle="活动表格"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            onClick={() => {
              onClickDraw('create');
            }}
            type="primary"
          >
            新建
          </Button>,
        ]}
      />
      <StudentCreateOrEdit
        open={isOpen}
        onOpenChange={setIsOpen}
        title={drawType}
        positionList={position}
        classList={classInfo}
        editData={editData}
      />
    </PageContainer>
  );
};
export default StudentPage;
