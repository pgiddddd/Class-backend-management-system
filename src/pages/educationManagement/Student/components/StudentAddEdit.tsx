import { addStudent, editStudent } from '@/services/student';
import { message } from '@/utils/escapeAntd';
import type { ProFormInstance } from '@ant-design/pro-components';
import {
  DrawerForm,
  ProForm,
  ProFormCascader,
  ProFormSelect,
  ProFormText,
} from '@ant-design/pro-components';
import { useEffect, useMemo, useRef } from 'react';
interface StudentProps {
  //打开的状态
  open: boolean;
  onOpenChange: (isOpen: boolean) => void;
  //表格的标题
  title: string;
  positionList: Array<{ label: string; value: number }>;
  classList: Array<any>;
  editData?: StudentAPI.StudentAddOrEditType;
}
const StudentCreateOrEdit = (props: StudentProps) => {
  const formRef = useRef<ProFormInstance>();
  //设置标题
  const drawTypeObj: { [key: string]: string } = {
    create: '新增学生',
    edit: '编辑学生',
  };
  //赋值模态框的标题
  const drawTitle = useMemo(() => {
    return drawTypeObj[props.title] || '';
  }, [props.title]);

  useEffect(() => {
    formRef.current?.setFieldsValue({
      ...props.editData,
      areaList: [props.editData?.facultyId, props.editData?.classInfoId],
    });
  }, [props.editData]);

  useEffect(() => {
    if (props.open === false) {
      formRef.current?.resetFields();
    }
  }, [props.open]);

  const handleFinish = async (values: any) => {
    const fn = props.title === 'create' ? addStudent : editStudent;
    const data = await fn({
      ...values,
      classInfo: values.areaList[1],
    });
    message.info(data);
    props.onOpenChange(false);
  };
  return (
    <>
      <DrawerForm
        formRef={formRef}
        title={drawTitle}
        open={props.open}
        onOpenChange={(value: boolean) => props.onOpenChange(value)}
        onFinish={handleFinish}
      >
        <ProFormText
          label="学号"
          name="studentAccount"
          width="lg"
          placeholder="请输入学号"
          rules={[{ required: true, message: '请输入工号' }]}
        />
        <ProFormText
          label="姓名"
          name="studentName"
          width="lg"
          placeholder="请输入姓名"
          rules={[{ required: true, message: '请输入姓名' }]}
        />
        <ProForm.Group>
          <ProFormSelect
            options={props.positionList}
            width="xs"
            name="position"
            label="职位"
            rules={[{ required: true, message: '请输入选择职位' }]}
            initialValue={0}
          />
          <ProFormCascader
            width={200}
            name="areaList"
            label="班级"
            rules={[{ required: true, message: '请选择班级' }]}
            fieldProps={{
              options: props.classList,
              fieldNames: {
                children: 'classInfoList',
                value: 'id',
                label: 'name',
              },
            }}
          />
        </ProForm.Group>
      </DrawerForm>
    </>
  );
};
export default StudentCreateOrEdit;
