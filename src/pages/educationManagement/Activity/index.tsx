import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { PageContainer, ProTable } from '@ant-design/pro-components';
import { Button } from 'antd';
import { useRef } from 'react';

const columns: ProColumns[] = [
  {
    title: '活动编号',
    dataIndex: 'id',
  },
  {
    title: '活动名称',
    dataIndex: 'name',
    copyable: true,
    ellipsis: true,
    tooltip: '标题过长会自动收缩',
    formItemProps: {
      rules: [
        {
          required: true,
          message: '此项为必填项',
        },
      ],
    },
  },
  {
    title: '开始时间',
    key: 'startTime',
    dataIndex: 'created_at',
    valueType: 'date',
    hideInSearch: true,
  },
  {
    title: '结束时间',
    key: 'endTime',
    dataIndex: 'created_at',
    valueType: 'date',
    hideInSearch: true,
  },
  {
    title: '时间',
    dataIndex: 'created_at',
    valueType: 'dateRange',
    hideInTable: true,
    search: {
      transform: (value) => {
        return {
          startTime: value[0],
          endTime: value[1],
        };
      },
    },
  },
  {
    title: '届数',
    dataIndex: 'grade',
  },
  {
    title: '操作',
    valueType: 'option',
    key: 'option',
    render: () => [
      <a key="edit">编辑</a>,
      <a key="view">详情</a>,
      <a key="delete">删除</a>,
    ],
  },
];

const ActivityPage = () => {
  const actionRef = useRef<ActionType>();
  return (
    <PageContainer title={false}>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 'auto',
        }}
        options={false}
        pagination={{
          pageSizeOptions: [5, 10, 15, 20],
          showSizeChanger: true,
        }}
        dateFormatter="string"
        headerTitle="活动表格"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            onClick={() => {
              actionRef.current?.reload();
            }}
            type="primary"
          >
            新建
          </Button>,
        ]}
      />
    </PageContainer>
  );
};
export default ActivityPage;
