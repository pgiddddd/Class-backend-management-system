import { createTeacher, updateTeacher } from '@/services/teacher';
import { message } from '@/utils/escapeAntd';
import type { ProFormInstance } from '@ant-design/pro-components';
import {
  DrawerForm,
  ProFormRadio,
  ProFormText,
} from '@ant-design/pro-components';
import { useEffect, useMemo, useRef } from 'react';

interface TeacherProps {
  //打开的状态
  open: boolean;
  onOpenChange: (isOpen: boolean) => void;
  //表格的标题
  title: string;
  editData?: TeacherAPI.TeacherAddEditType;
}
const menuTypesOptions = [
  { label: '启用', value: 1 },
  { label: '禁用', value: 2 },
];

const TeacherCreateOrEdit = (props: TeacherProps) => {
  const formRef = useRef<ProFormInstance>();
  const drawTypeObj: { [key: string]: string } = {
    create: '新增教师',
    edit: '编辑教师',
  };

  const drawTitle = useMemo(() => {
    return drawTypeObj[props.title] || '';
  }, [props.title]);

  useEffect(() => {
    formRef.current?.setFieldsValue({
      userAccount: props.editData?.teacherAccount,
      userName: props.editData?.teacherName,
    });
  }, [props.editData]);

  useEffect(() => {
    if (props.open === false) {
      formRef.current?.resetFields();
    }
  }, [props.open]);

  const handleFinish = async (values: TeacherAPI.TeacherAddEditType) => {
    const fn = props.title === 'create' ? createTeacher : updateTeacher;
    const data = await fn({
      ...values,
    });
    message.info(data);
    props.onOpenChange(false);
  };

  return (
    <>
      <DrawerForm<TeacherAPI.TeacherAddEditType>
        title={drawTitle}
        open={props.open}
        onOpenChange={(value: boolean) => props.onOpenChange(value)}
        onFinish={handleFinish}
        formRef={formRef}
      >
        <ProFormText
          label="工号"
          name="userAccount"
          width="lg"
          placeholder="请输入工号"
          rules={[{ required: true, message: '请输入工号' }]}
        />
        <ProFormText
          label="姓名"
          name="userName"
          width="lg"
          placeholder="请输入姓名"
          rules={[{ required: true, message: '请输入姓名' }]}
        />
        {props.title !== 'edit' && (
          <ProFormRadio.Group
            label="状态"
            name="status"
            initialValue={1}
            width="lg"
            options={menuTypesOptions}
            required
          />
        )}
      </DrawerForm>
    </>
  );
};
export default TeacherCreateOrEdit;
