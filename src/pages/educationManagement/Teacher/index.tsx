import { resetPassword } from '@/services/comom';
import { getAllFaculties } from '@/services/faculty';
import { deleteTeacherAndClass, getTeacherByPage } from '@/services/teacher';
import transformData from '@/utils/formatData';
import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { PageContainer, ProTable } from '@ant-design/pro-components';
import { Button, Modal, Popconfirm } from 'antd';
import { useEffect, useRef, useState } from 'react';
import TeacherCreateOrEdit from './components/TeacherCreateOrEdit';

const TeacherPage = () => {
  const { confirm } = Modal;
  const actionRef = useRef<ActionType>();
  //控制编辑和新增的开关
  const [isOpen, setIsOpen] = useState(false);
  //新增还是编辑
  const [drawType, setDrawType] = useState<'create' | 'edit'>('edit');
  const [editData, setEditData] = useState<TeacherAPI.TeacherAddEditType>();
  //学院下拉框
  const [faculties, setFaculties] = useState<
    { label: string; value: number }[]
  >([]);

  const onClickDraw = (
    type: 'edit' | 'create',
    data?: TeacherAPI.TeacherAddEditType,
  ) => {
    setDrawType(type);
    if (type !== 'create') {
      setEditData(data);
    }
    setIsOpen(!isOpen);
  };
  useEffect(() => {
    if (isOpen === false) {
      actionRef.current?.reload();
    }
  }, [isOpen]);
  //获取学院下拉框
  const getFaculties = async () => {
    const arr = await transformData(getAllFaculties);
    setFaculties(arr);
  };

  useEffect(() => {
    getFaculties();
  }, []);

  const columns: ProColumns[] = [
    {
      title: '工号',
      dataIndex: 'teacherAccount',
    },
    {
      title: '姓名',
      dataIndex: 'teacherName',
    },
    {
      title: '关联班级',
      dataIndex: 'classInfo',
      hideInSearch: true,
      render: (_, record) => {
        const list = record.classinfo.map((item: any) => item.name).join('、');
        return <>{list}</>;
      },
    },
    {
      title: '二级学院',
      dataIndex: 'facultiesId',
      valueType: 'select',
      fieldProps: {
        options: faculties,
      },
      hideInTable: true,
    },
    {
      title: '所属二级学院',
      dataIndex: 'facultyName',
      hideInSearch: true,
      // hideInForm: true,
    },
    {
      title: '操作',
      valueType: 'option',
      key: 'option',
      render: (_, entity) => [
        <>
          <a key="edit" onClick={() => onClickDraw('edit', entity)}>
            编辑
          </a>
          <a
            key="reset"
            onClick={() => {
              confirm({
                title: '重置密码',
                icon: <ExclamationCircleOutlined />,
                content: '重置密码为123456',
                onOk: async () => {
                  await resetPassword(entity.teacherAccount);
                },
              });
            }}
          >
            重置密码
          </a>
          <Popconfirm
            title="是否删除该教师"
            okText="确认"
            cancelText="取消"
            onConfirm={async () => {
              // console.log(entity);
              console.log(entity.classinfo);
              let classInfoArr = [] as any;
              entity.classinfo?.forEach((item: classInfoAPI.classInfoItem) => {
                console.log(item);
                classInfoArr.push(item.id);
              });

              await deleteTeacherAndClass({
                classId: classInfoArr,
                teacherAccount: entity.teacherAccount,
              });
            }}
          >
            <a key="delete">删除</a>
          </Popconfirm>
        </>,
      ],
    },
  ];
  return (
    <PageContainer title={false}>
      <ProTable
        columns={columns}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 'auto',
        }}
        options={false}
        pagination={{
          pageSizeOptions: [5, 10, 15, 20],
          showSizeChanger: true,
        }}
        request={async (params) => {
          const data = await getTeacherByPage({
            pageSize: params.pageSize as number,
            pageNumber: params.current as number,
            ...params,
          });
          return {
            data: data.records,
            total: data.total,
          };
        }}
        dateFormatter="string"
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            onClick={() => {
              onClickDraw('create');
            }}
            type="primary"
          >
            新建
          </Button>,
        ]}
      />
      <TeacherCreateOrEdit
        open={isOpen}
        onOpenChange={setIsOpen}
        title={drawType}
        editData={editData}
      />
    </PageContainer>
  );
};
export default TeacherPage;
