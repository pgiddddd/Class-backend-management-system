import { changePwd } from '@/services/login';
import { message } from '@/utils/escapeAntd';
import type { ProFormInstance } from '@ant-design/pro-components';
import { ProForm, ProFormText } from '@ant-design/pro-components';
import { history } from '@umijs/max';
import { useRef } from 'react';
import styles from './index.less';

const ChangePwdPage = () => {
  const formRef = useRef<ProFormInstance>();
  const handleFinsh = async (values: LoginAPI.ChangePwdAPI) => {
    const info = JSON.parse(localStorage.getItem('loginInfo') || '');

    const params = {
      ...values,
      userAccount: info.userAccount,
    };
    const res = await changePwd(params);
    if (res === '修改成功') {
      message.success(res);
      localStorage.removeItem('userInfo');
      history.push('/login');
    }
    formRef.current?.resetFields();
  };
  return (
    <div className={styles.headerWrapper}>
      <div className={styles.header}>
        <div className={styles.text}>
          <p>Retrieve the password</p>
          <p>修改密码</p>
        </div>
      </div>
      <div className={styles.formWrapper}>
        <div className={styles.form}>
          <ProForm onFinish={handleFinsh} formRef={formRef}>
            <ProFormText.Password
              label="旧密码"
              name="oldPwd"
              width="lg"
              placeholder="请输入密码"
              rules={[{ required: true, message: '请输入密码' }]}
            />
            <ProFormText.Password
              label="新密码"
              name="newPwd"
              width="lg"
              placeholder="请输入密码"
              rules={[{ required: true, message: '请输入密码' }]}
            />
          </ProForm>
        </div>
      </div>
    </div>
  );
};
export default ChangePwdPage;
