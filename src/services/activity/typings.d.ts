declare namespace ActivityAPI {
  interface ActivityItem {
    //活动编号
    id: number;
    //活动名称
    name: string;
    //活动描述
    description: string;
    //活动开始时间
    startTime: string;
    //获取到结束时间
    endTime: string;
    // 活动地点
    location: string;
  }
}
