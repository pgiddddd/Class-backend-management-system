import { request } from '@umijs/max';
const apiName = {
  getClassInfo: '/class-info/getAllClassInfo',
};
export const getClass = () => {
  return request(apiName.getClassInfo, {
    method: 'get',
  });
};
