declare namespace classInfoAPI {
  interface classInfoItem {
    id: number;
    name: string;
    teacher: string;
    grade: number;
    faculties: number;
  }
}
