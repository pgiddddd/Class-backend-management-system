import { request } from '@umijs/max';
const apiName = {
  createTeacher: '/teacher-info/addTeacher',
  getAllTeacher: '/teacher-info/getAllTeacher',
  getAllTeacherByPage: '/teacher-info/getTeacherByPage',
  deleteTeacher: '/teacher-info/deleteTeacherAndClass',
  editTeacher: '/teacher-info/updateTeacher',
};

export const createTeacher = (data: TeacherAPI.TeacherAddEditType) => {
  return request(apiName.createTeacher, {
    method: 'post',
    data,
  });
};
//获取所有老师
export const getTeacher = () => {
  return request(apiName.getAllTeacher, {
    method: 'get',
  });
};
//获取老师全部信息
export const getTeacherByPage = (data: TeacherAPI.TeacherItem) => {
  return request(apiName.getAllTeacherByPage, {
    method: 'post',
    data,
  });
};
//删除老师信息
export const deleteTeacherAndClass = (data: TeacherAPI.TeacherDelItem) => {
  return request(apiName.deleteTeacher, {
    method: 'delete',
    data,
  });
};
//修改老师信息
export const updateTeacher = (data: TeacherAPI.TeacherAddEditType) => {
  return request(apiName.editTeacher, {
    method: 'post',
    data,
  });
};
