declare namespace TeacherAPI {
  interface TeacherAddEditType {
    //教师工号
    teacherAccount: string;
    userName?: string;
    userAccount?: string;
    teacherName: string;
    permission?: number;
    status?: number;
  }
  interface TeacherItem {
    classInfo?: string[];
    facultiesName?: number;
    grade?: number;
    pageNumber: number;
    pageSize: number;
    teacherAccount?: string;
    teacherName?: string;
  }
  interface TeacherDelItem {
    classId: string[];
    teacherAccount: string;
  }
}
