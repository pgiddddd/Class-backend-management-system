import { request } from '@umijs/max';
const apiName = {
  getAll: '/faculties/getAllFaculties',
};
export const getAllFaculties = () => {
  return request(apiName.getAll, {
    method: 'get',
  });
};
