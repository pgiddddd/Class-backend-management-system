import { request } from '@umijs/max';
const apiName = {
  resetPwd: '/user/resetPwd',
  getAllGradeUrl: '/class-info/getAllGrade',
};
// 重置密码
export const resetPassword = (data: string) => {
  return request(`${apiName.resetPwd}?userAccount=${data}`, { method: 'put' });
};

//获取全部届数
export const getAllGrade = () => {
  return request(apiName.getAllGradeUrl, {
    method: 'get',
  });
};
