import { request } from '@umijs/max';
const apiName = {
  getPositions: '/position/getPositions',
};

export const getPositionInfo = () => {
  return request(apiName.getPositions, {
    method: 'get',
  });
};
