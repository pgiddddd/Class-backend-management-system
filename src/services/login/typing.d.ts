declare namespace LoginAPI {
  interface LoginUserAPI {
    userAccount: string;
    password: string;
    permission: number;
    token: string;
  }
  interface ChangePwdAPI {
    userAccount: string;
    oldPwd: string;
    newPwd: string;
  }
}
