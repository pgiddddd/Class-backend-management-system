import { request } from '@umijs/max';

//登录
export const login = (data: LoginAPI.LoginUserAPI) =>
  request('/user/login', {
    method: 'post',
    data,
  });

//登出
export const logout = (data: any) => {
  return request(`/user/logOut?token=${data}`, {
    method: 'get',
  });
};

//修改密码
export const changePwd = (data: LoginAPI.ChangePwdAPI) => {
  return request('/user/changePwd', {
    method: 'post',
    data,
  });
};
