declare namespace StudentAPI {
  interface StudentItem {
    classInfoId?: number;
    facultiesId?: number;
    grade?: number;
    pageNumber: number;
    pageSize: number;
    studentAccount?: string;
    studentName?: string;
    teacherAccount?: string;
  }
  interface StudentAddOrEditType {
    classInfo: number;
    postion: number;
    studentAccount: string;
    studentName: string;
    classInfoId?: number;
    facultyId?: number;
  }
}
