import { request } from '@umijs/max';
const apiName = {
  getStudent: '/student-info/getStudentByPage',
  addStudent: '/student-info/addStudent',
  editStudent: '/student-info/updateStudent',
  deleteStudent: '/student-info/deleteStudent',
};
//获取全部学生
export const getStudent = (data: StudentAPI.StudentItem) => {
  return request(apiName.getStudent, {
    method: 'post',
    data,
  });
};
//增加学生
export const addStudent = (data: StudentAPI.StudentAddOrEditType) => {
  return request(apiName.addStudent, {
    method: 'post',
    data,
  });
};
//编辑学生
export const editStudent = (data: StudentAPI.StudentAddOrEditType) => {
  return request(apiName.editStudent, {
    method: 'post',
    data,
  });
};
//删除学生
export const deleteStudentByAccount = (data: string) => {
  return request(`${apiName.deleteStudent}?Account=${data}`, { method: 'put' });
};
