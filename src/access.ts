interface UserInfoInter {
  userAccount: string;
  userName: string;
  permission: number;
}

export default (initialState: UserInfoInter) => {
  // 参考文档 https://umijs.org/docs/max/access
  //1:班委 2:辅导员 3:超级管理员
  const { permission } = initialState;
  //学生表、辅导员表、活动表
  const canSeeAdmin = !!(initialState && permission === 3);
  //学生表（辅导员不显示辅导员字段、所属二级学院字段）、活动表
  const canSeeTeacherOrAdmin = !!(
    (initialState && permission === 2) ||
    permission === 3
  );
  return {
    canSeeAdmin,
    canSeeTeacherOrAdmin,
  };
};
