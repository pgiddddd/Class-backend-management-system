// 运行时配置

// 全局初始化数据配置，用于 Layout 用户信息和权限初始化
// 更多信息见文档：https://umijs.org/docs/api/runtime-config#getinitialstate
import Request from '@/utils/request';
import {
  EditTwoTone,
  FormOutlined,
  LogoutOutlined,
  SmileTwoTone,
} from '@ant-design/icons';
import { RunTimeLayoutConfig, history } from '@umijs/max';
import { Dropdown } from 'antd';
import { logout } from './services/login';

export async function getInitialState() {
  const userInfo = localStorage.getItem('userInfo');
  const userInfoResult = userInfo
    ? JSON.parse(userInfo as unknown as string)
    : {};
  return {
    ...userInfoResult,
  };
}

export const layout: RunTimeLayoutConfig = () => {
  const userInfo = JSON.parse(localStorage.getItem('userInfo') || '');
  return {
    logo: <EditTwoTone />,
    menu: {
      locale: false,
    },
    layout: 'mix',
    avatarProps: {
      src: <SmileTwoTone />,
      size: 'small',
      title: userInfo.userName,
      render: (props, dom) => {
        return (
          <Dropdown
            menu={{
              items: [
                {
                  key: 'logout',
                  icon: <LogoutOutlined />,
                  label: '退出登录',
                  onClick: async () => {
                    const token = localStorage.getItem('token') || '';
                    await logout(token);
                    history.push('/login');
                    sessionStorage.clear();
                    localStorage.clear();
                  },
                },
                {
                  key: 'changePwd',
                  icon: <FormOutlined />,
                  label: '更改密码',
                  onClick: async () => {
                    history.push('/changePwd');
                  },
                },
              ],
            }}
          >
            {dom}
          </Dropdown>
        );
      },
    },
  };
};
export const request = Request;
