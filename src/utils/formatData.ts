const transformData = async (fn: any) => {
  const data = await fn();
  const arr = Object.keys(data).map((key) => ({
    label: data[key],
    value: parseInt(key),
  }));
  return arr;
};
export default transformData;
