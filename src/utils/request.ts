import { RequestConfig } from '@umijs/max';
import { message } from './escapeAntd';
const status = {
  ok: 100200,
};

const successFn = async (response: any) => {
  let { data } = response;
  if (data.code !== status.ok) {
    throw message.error(data.msg);
  }
  return response.data;
};

const request: RequestConfig = {
  timeout: 20000,
  baseURL: '/api',
  responseInterceptors: [
    [
      successFn as any,
      (e) => {
        message.error('后台错误，请联系管理员');
        throw e;
      },
    ],
  ],
};
export default request;
